<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bringnet_brelades');

/** MySQL database username */
define('DB_USER', 'bringnet_dba');

/** MySQL database password */
define('DB_PASSWORD', '%0D9B*+JNwVK');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '4f]%K=F_EG<SAz3kLi)dv<WUg!0pEo~z4yG(*E`4+aAX:)ETf5F-#_Jvlw= ~JVv');
define('SECURE_AUTH_KEY',  '_x:Yq6/I<ZT/.~qo/QD@.xN10nyCRTDn)dh]kTH&k3]u Cf%FBnit3S~a0iYy(+x');
define('LOGGED_IN_KEY',    'h=jJx@>ht2FPSGnNh)F72#tS 9#8U@f>R4lUm:>L-cle6Ai3+Q%gmJrtL KFjt+B');
define('NONCE_KEY',        'te37P9)Eq|ya;t4VUUhGWL>)|~y^>nvdQ n#uCNUH exb%5ypyN=pW57Qpd-lJJn');
define('AUTH_SALT',        'J3@wo&4,E>8hN.=Pvvg/Ce0us33;xbDu]tTzpB6OlzArcN$N##iX]t}^5;|AK,Yq');
define('SECURE_AUTH_SALT', '3_Bt`(O<bwdnfD.J3hn4`RbJ;!&NHKTk4gne/xU-(vs^*P}R*5@se8[Ak)]wMJX{');
define('LOGGED_IN_SALT',   'EJ!mB ^QVd_!y|wqR=ZJwqv_Hp>l^2(zTZA_)A~%*,GVI`f%lUpGUhUSm8jOsk/t');
define('NONCE_SALT',       'DVP#PIsVQZ!dvkW%<}zI~A~rHrw1<e1mtI7r,zC?a/ml,mTh,#l@@Uo_pptX*:f2');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
