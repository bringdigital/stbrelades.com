<?php
/*
Template Name: Blog
*/
get_header(); ?>


<?php 


		
$slugs = explode('/', get_query_var('category_name'));
$currentCategory = get_category_by_slug('/'.end($slugs));
	

//dd($currentCategory);

$args = array(
    'numberposts' => 1,
    'offset' => 0,
    'category' => $currentCategory->cat_ID,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'post',
    'post_status' => 'publish'
 );

$most_recent_post = wp_get_recent_posts($args, ARRAY_A)[0];


?>

<div class="wrapper">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<!-- ADVERT -->
				<img src="http://placehold.it/728x90" alt="">
			</div>
		</div>

		<article>
			<div class="row">
				<div class="col-sm-10">
					<h1><?php echo $most_recent_post['post_title']; ?></h1>

					<?php 
					if($currentCategory->name == "Videos"){
						?>
						<iframe width="560" height="315" src="https://www.youtube.com/embed/n69GS8_TFEM" frameborder="0" allowfullscreen></iframe>
						<?php
					}
					?>


				</div>
				<div class="col-sm-2">
					<a href="#" class="btn btn-default"><i class="fa fa-arrow-right fa-3x"></i></button></a>
				</div>
			</div>
		</article>

	</div>
</div>

		
<?php get_footer(); ?>