<?php
/*
Template Name: Rooms & Suites
*/
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header(); ?>

<?php
if ( have_posts() ) : while ( have_posts() ) : the_post();	
?>

<div class="jumbotron">
	<div class="container">
		<h1><?php
		the_title();
		?></h1>
	</div>
</div>

<section>
	<div class="container text-center">

		<div class="content-strip">
			<div class="row">
				<div class="col-xs-12">
					<?php
					the_content();
					?>
				</div>
			</div>
		</div>
			
				
	</div>
</section>

<?php
endwhile; endif;
?>

<div class="container-fluid boxes-container">
	<div class="row">
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box1.jpg)">
				<span class="valign-middle"><span>Rooms & Suites</span></span>
				<div class="desc">
					<p>Standard Family rooms offer functional space to make you family vacation a special one. One large double bedroom, an interconnected single bedroom and a shared bathroom. Beds are able to be configured to suit your needs with cots available for our younger guests. We recommend these rooms for families of three or Four people. The rooms feature tea and coffee making facilities and Flat screen wall mounted TV’s.</p>
					<p><a href="#" class="btn btn-default">Book Now</a></p>
				</div>
			</div>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box2.jpg)">
				<span class="valign-middle"><span>Dining</span></span>
			</div>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box5.jpg)">
				<span class="valign-middle"><span>Families</span></span>
			</div>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box3.jpg)">
				<span class="valign-middle"><span>Wellbeing & Fitness</span></span>
			</div>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box4.jpg)">
				<span class="valign-middle"><span>Wedding & Celebrations</span></span>
			</div>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<div class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box6.jpg)">
				<span class="valign-middle"><span>Special Offers</span></span>
			</div>
		</div>
	</div>
</div>

<?php
	require('partials/home-about-faq-contact.php');
?>

<?php get_footer(); ?>