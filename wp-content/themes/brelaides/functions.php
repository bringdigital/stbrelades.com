<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */

if ( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus( 
		array(
				'top_nav_main_menu' => 'Top Nav Main Menu',
				'footer_nav_menu' => 'Footer Nav Menu',
				'mobile_nav_menu' => 'Mobile Nav Menu'
			)
	);

}

add_filter('show_admin_bar', '__return_false');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

function dd($obj){
	echo "<pre>", var_dump($obj) ,"</pre>";
	die();
}

?>