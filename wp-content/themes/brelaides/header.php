<?php
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
//require_once('wp_bootstrap_navwalker.php');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="<?php bloginfo('html_type'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('&laquo;', true, 'right'); ?></title>
    <meta name="Author" content="@liamjoco" />
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

    <meta name="format-detection" content="telephone=no">
    
    <link rel="shortcut icon" href="<?php echo esc_url( get_template_directory_uri() );?>/images/favicon.ico" type="image/x-icon"/>

    <!-- Jquery -->
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js"></script>

    <!-- Font Awesome -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Slidebars CSS -->
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/css/slidebars.css">

    <!-- Hover Buttons -->
    <link rel="stylesheet" href="<?php echo esc_url( get_template_directory_uri() );?>/css/hover-min.css">


    <link href="<?php echo esc_url( get_template_directory_uri() );?>/css/theme.css" rel="stylesheet">

    <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

    <?php wp_head(); ?>
     <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->


    <!-- TAG MANAGER -->


  </head>
  <body <?php body_class(); ?>>

    <div id="sb-site">

        <header>
            <div class="container large-menu">
                <a class="logo" href="/">
                    <!-- <img src="<?php echo esc_url( get_template_directory_uri() );?>/images/logo.png" alt=""> -->
                    <img src="<?php the_field('header_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?> logo">
                </a>
                <div class="main-menu">
                    <?php 
                    $headerNav = array('theme_location' => 'top_nav_main_menu', 
                                        'menu' => 'top_nav_main_menu');

                    wp_nav_menu($headerNav); 
                    ?>
                    <!-- <ul> -->
                       <!--  <li>
                            <a href="">Home</a>
                        </li> -->
<!--                         <li>
                            <a href="">Rooms</a>
                        </li>
                        <li>
                            <a href="">Dining</a>
                        </li>
                        <li>
                            <a href="">Facilities</a>
                        </li>
                        <li>
                            <a href="">Conferences</a>
                        </li>
                        <li>
                            <a href="">Offers</a>
                        </li>
                        <li>
                            <a href="" class="hvr-float-shadow">Book Now</a>
                        </li>
                    </ul>
 -->                </div>
            </div>

            <div class="container-fluid mobile-menu">
            
                <div class="toggle-menu">
                    <div class="sb-toggle-left toggle-mobile-menu">
                        <span class="left"></span>
                        <span class="center"></span>
                        <span class="right"></span>
                    </div>
                </div>
                <div class="mobile-logo">
                    <a href="#">
                        <img src="<?php the_field('sidebar_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?> logo">
                    </a>
                </div>
                <div class="mobile-header-icons">
                    <a href="#"><i class="fa fa-map-marker fa-2x"></i></a>
                    <a href="#"><i class="fa fa-phone fa-2x"></i></a>
                </div>

            </div>
                
        </header>

        <div class="torso">

<?php //<!--header end-->?>
