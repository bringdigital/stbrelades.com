		
		</div> <!-- END OF TORSO -->

		<footer>
			<div class="top-footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 text-center">
							<img src="<?php the_field('footer_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?> logo">
						</div>
					</div>
				</div>
			</div>
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-12">
							<div class="bottom-footer-row footer-menu">
								<?php 
								$footerNav = array('theme_location' => 'footer_nav_menu', 
								              		'menu' => 'footer_nav_menu');

								wp_nav_menu($footerNav); 
								?>
								<!--
								<ul>
									<li>
										<a href="#">About The Hotel</a>
									</li>
									<li>
										<a href="#">Contact Us</a>
									</li>
									<li>
										<a href="#">FAQs</a>
									</li>
									<li>
										<a href="#">Events & Social Hub</a>
									</li>
									<li>
										<a href="#">Privacy Policy</a>
									</li>
									<li>
										<a href="#">T&Cs</a>
									</li>
								</ul>
								-->
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="bottom-footer-row contact">
								<span><?php the_field('copyright_text', 'option'); ?></span>
								<span><?php the_field('footer_phone_title', 'option'); ?> <?php the_field('footer_phone_no', 'option'); ?></span>
								<span><?php the_field('footer_fax_title', 'option'); ?> <?php the_field('footer_fax_no', 'option'); ?></span>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="bottom-footer-row address">
								<p><?php the_field('footer_address', 'option'); ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>


		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() );?>/js/bootstrap.min.js"></script>

		<!-- Slidebars -->
		<script src="<?php echo esc_url( get_template_directory_uri() );?>/js/slidebars.js"></script>
		
		<script type="text/javascript" src="<?php echo esc_url( get_template_directory_uri() );?>/js/custom.js"></script>

		<?php wp_footer();?>

	</div><!-- END OF #SB-SITE -->

	<div class="sb-slidebar sb-left">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 text-center">
					<a href="#" class="sidebar-logo">
						<img src="<?php echo esc_url( get_template_directory_uri() );?>/images/sidebar-logo-white.png" alt="">
					</a>

					<div class="sidebar-menu">
						<ul>
	                        <li>
	                            <a href="/">Home</a>
	                        </li>
	                        <li>
	                            <a href="#">Rooms</a>
	                        </li>
	                        <li>
	                            <a href="#">Dining</a>
	                        </li>
	                        <li>
	                            <a href="#">Facilities</a>
	                        </li>
	                        <li>
	                            <a href="">Conferences</a>
	                        </li>
	                        <li>
	                            <a href="#">Offers</a>
	                        </li>
	                        <li>
	                            <a href="#">Book Now</a>
	                        </li>
	                    </ul>
					</div>

					<div class="sidebar-finish">
						<img src="<?php echo esc_url( get_template_directory_uri() );?>/images/sidebar-finish.png" alt="">
					</div>

				</div>
			</div>
		</div>
	</div>

</body>

</html>