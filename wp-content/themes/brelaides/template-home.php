<?php
/*
Template Name: Home
*/
/**
 * @package WordPress
 * @subpackage Default_Theme
 */
get_header(); ?>

<?php
if ( have_posts() ) : while ( have_posts() ) : the_post();	
?>

<div class="jumbotron">
	<div class="container">
		<h1><?php
		the_title();
		?></h1>
	</div>
</div>

<section>
	<div class="container text-center">

		<div class="row">
			<div class="col-xs-12">
<!-- 				<p>
					"Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum natus sunt id cupiditate ratione voluptas sequi veritatis iste, ea necessitatibus sit, molestias voluptate officia laudantium dolorem hic recusandae minus reprehenderit."
				</p>
 -->
				<?php
				the_content();
				?>
		
				<p>
					<a href="/bookings" class="btn btn-default btn-lg hvr-shutter-out-horizontal">Prices &amp; Availability</a>
				</p>
			</div>
		</div>
				
	</div>
</section>

<?php
endwhile; endif;
?>

<div class="container-fluid boxes-container">
	<div class="row">
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box1.jpg)">
				<span class="valign-middle"><span>Rooms &amp; Suites</span></span>
			</a>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box2.jpg)">
				<span class="valign-middle"><span>Dining</span></span>
			</a>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box5.jpg)">
				<span class="valign-middle"><span>Families</span></span>
			</a>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box3.jpg)">
				<span class="valign-middle"><span>Wellbeing &amp; Fitness</span></span>
			</a>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box4.jpg)">
				<span class="valign-middle"><span>Wedding &amp; Celebrations</span></span>
			</a>
		</div>
		<div class="box-container col-xs-6 col-md-4">
			<a href="#" class="box" style="background-image:url(<?php echo esc_url( get_template_directory_uri() );?>/images/box6.jpg)">
				<span class="valign-middle"><span>Special Offers</span></span>
			</a>
		</div>
	</div>
</div>

<?php
	require('partials/home-about-faq-contact.php');
?>

<?php get_footer(); ?>