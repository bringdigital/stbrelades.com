<section class="home-about-faq-contact">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 text-center">
				<img src="<?php echo esc_url( get_template_directory_uri() );?>/images/bar.png" class="img-responsive bar-splitter" alt="">
			</div>
		</div>
		<div class="row">
			<div class="col-sm-4 text-center">
				<div class="part">
					<h2><?php the_field('snippet_1_title'); ?></h2>
					<p><?php the_field('snippet_1_content'); ?></p>
					<p><a class="btn btn-default btn-lg hvr-shutter-out-horizontal" href="<?php the_field('snippet_1_link'); ?>"><?php the_field('snippet_1_link_text'); ?></a></p>
				</div>
			</div>
			<div class="col-sm-4 text-center">
				<div class="part">	
					<h2><?php the_field('snippet_2_title'); ?></h2>
					<p><?php the_field('snippet_2_content'); ?></p>
					<p><a class="btn btn-default btn-lg hvr-shutter-out-horizontal" href="<?php the_field('snippet_2_link'); ?>"><?php the_field('snippet_2_link_text'); ?></a></p>
				</div>
			</div>
			<div class="col-sm-4 text-center">
				<div class="part">	
					<h2><?php the_field('snippet_3_title'); ?></h2>
					<?php the_field('snippet_3_content'); ?>
					<!--<p>
						St Brelade's Bay Hotel<br>
						La Route de la Baie<br>
						St Brelade<br>
						Jersey CI<br>
						JE3 8EF<br>
					</p>
					<p>Phone: +44 (0)1534 746 141</p>
					<p>Fax: +44 (0)1534 747 278</p>
					<p class="email">Email: info@stbreladesbayhotel.com</p>-->
				</div>
			</div>
		</div>
	</div>
</section>
